# SQL_OLvoyage


### Contexte : la société OLVoyage

Acteur majeur du transport européen, OLVoyage développe ses services autour de son cœur de métier, le ferroviaire.

Créée en 1938, OLVoyage est une société anonyme d’économie mixte dont l’État possède 51 % du capital, les 49 % restants appartenant aux actionnaires des sociétés financières ayant succédées aux cinq anciennes compagnies.

En 2017, le chiffre d’affaires de la société a atteint 22 059 M€. Ce qui représente une hausse de 5,9 %.

### Les objectifs

Dans le cadre de la rédaction du rapport d’activité annuel, OLVoyage souhaite synthétiser l’ensemble des informations relatives à ses services.

Il s’agit de créer les rapports dont le contenu a été prédéfini par la société. De ce fait, il est nécessaire de suivre les consignes détaillées dans les spécifications techniques.

Ces rapports devront se baser sur le modèle de base de données et les enregistrements fournis par la société.


### Spécifications techniques

1. Vous devez utiliser une machine virtuelle avec l’installation d’une base de données Oracle 10g configurée. Vous pouvez utiliser la même machine que pour les TP ou en utiliser une nouvelle.

2. Dans tous les cas, vous devez mettre votre schéma OLVoyage à jour.
3. Bien entendu, toutes les ressources telles que le web, manuels, cours, etc. sont à votre disposition.
4. Il est recommandé d’utiliser le client en ligne de commande pour la création de vos rapports.

### Rapports

Chaque rapport doit être généré au format HTML. Vous utiliserez pour cela des commandes SQL*Plus pour générer et enregistrer le code HTML contenant les résultats de vos requêtes.*

**Rappel :**

Les fichiers HTML doivent être nommés de la manière suivante : report_NN.htm. NN étant le numéro du rapport sur deux chiffres.

Une fois les fichiers générés, vous pouvez appliquer une feuille de style CSS afin de rendre les rapports plus lisibles. Nommez le fichier style.css. Cependant, la mise en forme de vos rapports doit rester sobre.

Quelques points à respecter :

    Donnez des noms explicites aux en-têtes de colonnes à l’affichage
    Il est primordial de sauvegarder chaque requête SQL associée aux commandes SQL*Plus dans un fichier de script. Veuillez utilise le même nommage que pour les fichiers HTML en utilisant .sql comme extension des fichiers (report_01.sql).
    Si dans un rapport il vous est demandé de récupérer le nom et le prénom d’une personne, veuillez afficher ces deux informations dans une seule colonne. Ceci allègera l’affichage.
    Les noms des personnes doivent toujours apparaitre en majuscules sur les rapports.
    Le nom d’un train est composé de son numéro, sa ville de départ et sa ville d’arrivée.
