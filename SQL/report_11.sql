SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_11</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_11.html 
  
TITLE "- Trains de Paris -" 

SELECT t.train_id AS "Numéro", s.city AS "NOM (Dir)", pass_name AS "Titre", ROUND((t.price * (1-(discount_pct/100))), 2) AS "Tarif Réduit Semaine", ROUND((t.price * (1-(discount_we_pct/100))), 2) AS "Tarif Réduit Week-End"
FROM t_train t
CROSS JOIN t_pass
JOIN t_station s ON s.station_id = t.arrival_station_id
JOIN t_station s2 ON s2.station_id = t.departure_station_id
WHERE s2.city = 'Paris'
ORDER BY t.train_id;


SET markup html off spool off 
spool off