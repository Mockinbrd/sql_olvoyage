SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_07</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_07.html 
  
TITLE "- Top-5 des trains -" 

SELECT *
FROM (SELECT t.train_id AS "Numero", s.city || ' - ' || s2.city AS "Train", COUNT(r.buyer_id) AS "Total"
	FROM t_train t
	JOIN t_station s ON t.departure_station_id = s.station_id
	JOIN t_station s2 ON t.arrival_station_id = s2.station_id
	JOIN t_wagon_train w ON t.train_id=w.train_id
	JOIN t_ticket ti ON ti.wag_tr_id = w.wag_tr_id
	JOIN t_reservation r ON ti.reservation_id = r.reservation_id
	WHERE r.buyer_id IS NOT NULL
	GROUP BY t.train_id, s.city, s2.city
	ORDER BY COUNT(r.reservation_id) DESC)
WHERE ROWNUM <=5;


SET markup html off spool off 
spool off