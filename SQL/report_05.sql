SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_05</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_05.html 
  
TITLE "- Trajets effectués -" 

SELECT t.train_id AS "Numéro du Train", d.city || TO_CHAR(t.departure_time, ' (DD/MM/YY HH24:MI) ') || ' - ' || a.city || TO_CHAR(t.arrival_time, ' (DD/MM/YY HH24:MI) ') AS "Trajets", t.distance, t.price AS "Prix Initial"
FROM T_train t
JOIN t_station d on d.station_id = t.departure_station_id
JOIN t_station a on a.station_id = t.arrival_station_id;


SET markup html off spool off 
spool off