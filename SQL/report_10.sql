SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_10</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_10.html 
  
TITLE "- Réservations « Séniors » de mars 2019 -" 

SELECT COUNT(c.customer_id) AS "Nb Réservation Senior de Janv"
FROM t_customer c
JOIN t_pass p ON p.pass_id = c.pass_id
JOIN t_reservation r ON r.buyer_id = c.customer_id
JOIN t_ticket ti ON ti.reservation_id = r.reservation_id
JOIN t_wagon_train w ON w.wag_tr_id = ti.wag_tr_id
JOIN t_train t ON t.train_id = w.train_id
WHERE p.pass_name LIKE 'Senior'
AND t.departure_time LIKE ('__/01/19')
AND r.buy_method IS NOT NULL;


SET markup html off spool off 
spool off