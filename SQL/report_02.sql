SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_02</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_02.html 
  
TITLE "- Acheteurs réservistes sans billet -" 

SELECT concat(c.first_name, c.last_name) as CLIENTS
FROM t_customer c
JOIN t_pass p ON c.pass_id = p.pass_id
WHERE lower(p.pass_name) <> lower(c.last_name)
ORDER BY CLIENTS;


SET markup html off spool off 
spool off