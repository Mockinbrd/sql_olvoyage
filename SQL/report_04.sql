SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_04</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_04.html 
  
TITLE "- Titres d’abonnement -" 

select upper(c.last_name) || ' ' || c.first_name as CLIENTS, p.pass_name as "Titre d'abonnement",c.pass_date,
CASE
WHEN add_months(c.pass_date,12) IS NULL THEN 'Aucun'
WHEN add_months(c.pass_date,12) < to_date('15-Mai-2019') then 'Périmé !'
END "Abonnement valide?"
FROM t_customer c
JOIN t_pass p ON (c.pass_id = p.pass_id)

UNION ALL

SELECT UPPER(last_name)||' '|| first_name as CLIENTS ,to_char(null) as "Titre d'abonnement",pass_date,
CASE
WHEN add_months(pass_date,12) IS NULL THEN 'Aucun'
WHEN add_months(pass_date,12) < to_date('15-Mai-2019') then 'Périmé !'
END "Abonnement valide?"
FROM t_customer
WHERE pass_date IS NULL
ORDER BY "CLIENTS";


SET markup html off spool off 
spool off