SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_15</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_15.html 
  
TITLE "- Organigramme -" 

SELECT
	lpad(last_name || ' ' || e.first_name, length(last_name || ' ' || e.first_name) + (LEVEL)-2, '_') "Nom",
	(SELECT count(r.reservation_id)
	FROM t_reservation r
	GROUP BY r.employee_id
	HAVING e.employee_id = r.employee_id) "Nombre de réservation"
FROM t_employee e
WHERE LEVEL != 1
START WITH e.manager_id is null
CONNECT BY PRIOR e.employee_id = e.manager_id


SET markup html off spool off 
spool off


