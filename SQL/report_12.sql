SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_12</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_12.html 
  
TITLE "- Popularité des abonnements -" 

SELECT p.pass_name AS "Titre les plus vendus", COUNT(c.customer_id) AS "Total"
FROM t_customer c
JOIN t_pass p ON p.pass_id=c.pass_id
GROUP BY p.pass_name
ORDER BY COUNT(c.customer_id) DESC;


SET markup html off spool off 
spool off