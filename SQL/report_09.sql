SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_09</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_09.html  
  
TITLE "- Vitesse moyenne d’un train -" 

SELECT t.train_id AS "Numero", s.city || '-' || s2.city AS "Train", ROUND((t.distance / (((TO_NUMBER(TO_CHAR(t.arrival_time, 'HH24')))+(((TO_NUMBER(TO_CHAR(t.arrival_time, 'MM')))/60)*100))-((TO_NUMBER(TO_CHAR(t.departure_time, 'HH24')))+(((TO_NUMBER(TO_CHAR(t.departure_time, 'MM')))/60)*100)))), 3) || 'km/h' AS "Vitesse"
FROM t_train t
JOIN t_station s ON t.departure_station_id = s.station_id
JOIN t_station s2 ON t.arrival_station_id = s2.station_id;


SET markup html off spool off 
spool off