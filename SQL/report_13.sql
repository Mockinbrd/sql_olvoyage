SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_13</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_13.html 
  
TITLE "- Billets des moins de 25 ans -" 

SELECT ti.ticket_id AS "Numéro", c.last_name || ' ' || c.first_name AS "Client", s.city || TO_CHAR(t.departure_time, ' (DD/MM/YY HH24:MI) ') || ' - ' || s2.city || TO_CHAR(t.arrival_time, ' (DD/MM/YY HH24:MI) ') AS "Train"
FROM t_train t
JOIN t_wagon_train wt ON wt.train_id = t.train_id
JOIN t_ticket ti ON ti.wag_tr_id = wt.wag_tr_id
JOIN t_reservation r ON r.reservation_id = ti.reservation_id
JOIN t_customer c ON c.pass_id = r.buyer_id
JOIN t_station s ON s.station_id = t.departure_station_id
JOIN t_station s2 ON s2.station_id = t.arrival_station_id
JOIN t_wagon w ON w.wagon_id = wt.wagon_id
JOIN t_pass p ON p.pass_id = c.pass_id
WHERE w.class_type = 1
AND r.buy_method IS NOT NULL
AND p.pass_name = '15-25'
AND t.departure_time BETWEEN TO_DATE ('15/01/19', 'dd/mm/yy') AND TO_DATE ('25/01/19', 'dd/mm/yy')
AND c.birth_date BETWEEN r.creation_date AND (add_months(r.creation_date, -300))
ORDER BY r.creation_date;


SET markup html off spool off 
spool off