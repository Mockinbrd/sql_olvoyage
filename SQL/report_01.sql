SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_01</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_01.html 
  
TITLE "- Meilleur employée -" 

SELECT * FROM (SELECT CONCAT(CONCAT(UPPER(last_name), ' '), first_name) employee 
FROM t_employee emp JOIN t_reservation res ON 
emp.employee_id=res.employee_id
GROUP BY emp.employee_id, emp.last_name, emp.first_name
ORDER BY COUNT(res.reservation_id)
DESC);


SET markup html off spool off 
spool off