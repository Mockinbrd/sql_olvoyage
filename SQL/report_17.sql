SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_17</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_17.html 
  
TITLE "- Paiement des réservations -" 

UPDATE t_reservation
SET r.buy_method = INITCAP('&Type_de_paiement'),
   t.price = (SELECT SUM((t.price)+
		((t.price*NVL(w.class_pct, 0))/100)-
                (((t.price+((t.price*NVL(w.class_pct, 0))/100))*NVL(CASE
          	WHEN TO_CHAR(t.departure_time, 'D') BETWEEN 1 AND 5 THEN p.discount_pct
          	WHEN TO_CHAR(t.departure_time, 'D') BETWEEN 6 AND 7 THEN p.discount_we_pct
          	END, 0))/100))
                   FROM t_reservation r
                   JOIN t_ticket ti ON r.reservation_id = ti.reservation_id
                   JOIN t_customer c ON c.customer_id = r.buyer_id
                   FULL OUTER JOIN t_pass p ON c.pass_id = p.pass_id
                   JOIN t_wagon_train wt ON ti.wag_tr_id = wt.wag_tr_id
                   JOIN t_train t ON wt.train_id = t.train_id
                   JOIN t_wagon w ON wt.wagon_id = w.wagon_id
                  WHERE r.reservation_id = &&num_de_reservation
                   GROUP BY r.reservation_id)
WHERE reservation_id = &Numero_de_reservation ;


SET markup html off
spool off