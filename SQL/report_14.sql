SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_14</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_14.html 
  
TITLE "- Places disponibles -" 

SELECT t.train_id AS "Numero", s.city || '-' || s2.city AS "Train", (sum(w.nb_seat) - sum(ti.seat)) AS "Places disponibles"
FROM t_train t
JOIN t_station s ON t.departure_station_id = s.station_id
JOIN t_station s2 ON t.arrival_station_id = s2.station_id
JOIN t_wagon_train wt ON t.train_id = wt.train_id
JOIN t_ticket ti ON ti.wag_tr_id = wt.wag_tr_id
JOIN t_wagon w ON wt.wagon_id = w.wagon_id
WHERE distance >= 300 
AND t.departure_time LIKE '22/01/2017'
AND ti.reservation_id IS NOT NULL
ORDER BY t.train_id;


SET markup html off spool off 
spool off