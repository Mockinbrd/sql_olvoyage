SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_08</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_08.html 
  
TITLE "- Clients sans abonnement -" 

SELECT UPPER(last_name)||' '||first_name as "Clients", address as "Adresse"
FROM t_customer
WHERE pass_id IS NULL
ORDER BY last_name , first_name;


SET markup html off spool off 
spool off