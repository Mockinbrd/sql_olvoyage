SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_06</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_06.html 
  
TITLE "- Statistiques -" 

SELECT
(SELECT COUNT(employee_id) 
	FROM t_employee) "Employé",
(SELECT COUNT(customer_id) 
	FROM t_customer) "Clients",
(SELECT ROUND(((SUM(NVL2(pass_id,1,0 )))/COUNT(customer_id)*100),0) 
	FROM t_customer) "Pourcentage Abonnés",
(SELECT COUNT(train_id) 
	FROM t_train) "Train",
(SELECT COUNT(station_id) 
	FROM t_station) "Station",
(SELECT COUNT(reservation_id) 
	FROM t_reservation) "Réservation",
(SELECT COUNT(ticket_id) 
	FROM t_ticket) "Ticket"
FROM dual;


SET markup html off spool off 
spool off