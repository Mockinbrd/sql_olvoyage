SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_03</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_03.html 
  
TITLE "- Première réservation effectuée -" 

SELECT concat(c.last_name, c.first_name) as CLIENTS, concat(e.last_name, e.first_name) as EMPLOYES, r.creation_date, r.reservation_id
FROM t_reservation r 
JOIN t_employee e ON r.employee_id=e.employee_id
JOIN t_customer c ON r.buyer_id=c.customer_id
WHERE r.creation_date = (select min(r.creation_date)
from t_reservation r);


SET markup html off spool off 
spool off