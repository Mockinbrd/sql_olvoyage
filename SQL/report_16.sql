SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Report_16</TITLE> - <style type='text/css'> - <link rel="stylesheet" href="css/bootstrap.css"> - TITLE {font-size: 40px; color: blue;}</style>"  
BODY "TEXT='#008000'" 
spool c:\report_16.html 
  
TITLE "- Augmentation des coordinateurs -" 

SELECT employee_id AS "Numero", first_name || ' ' || last_name AS "Employé", (salary+100) AS "Salaire augmenté (+100$)"
FROM t_employee
WHERE manager_id IN (SELECT employee_id from t_employee where manager_id is null);



SET markup html off spool off 
spool off